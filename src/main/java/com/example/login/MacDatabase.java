package com.example.login;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;


public class MacDatabase implements MacRepository{

    static Logger log = Logger.getLogger("Models/Database/MacDatabase.java");

    List<Mac> database = new ArrayList<Mac>();

    public MacDatabase(){
        FileHandler fileHandler;
        try {
            fileHandler = new FileHandler("MacDatabase.log");
            log.addHandler(fileHandler);
            SimpleFormatter formatter = new SimpleFormatter();
            fileHandler.setFormatter(formatter);
            log.info("Logger initialised");
        } catch (IOException e) {
            e.printStackTrace();
        }
        log.info("Database init!");

        Mac mac0 = new Mac(UUID.randomUUID(),"Apple",5000.59, 7);
        Mac mac1 = new Mac(UUID.randomUUID(),"Apple",4000.52, 5);
        Mac mac2 = new Mac(UUID.randomUUID(),"Apple",3000.57, 4);
        Mac mac3 = new Mac(UUID.randomUUID(),"Apple",2000.54, 3);
        Mac mac4 = new Mac(UUID.randomUUID(),"Apple",1000.51, 2);

        database.add(mac0);
        database.add(mac1);
        database.add(mac2);
        database.add(mac3);
        database.add(mac4);
    }

    @Override
    public Mac get(UUID id){
        for(Mac s :database){
            if(s.getUuid().compareTo(id)==0){
                return s;
            }
        }
        return null;
    }

    @Override
    public void add(Mac mac) {
        database.add(mac);
    }

    @Override
    public boolean update(Mac mac) {
        for (Mac s : database){
            if(s.getUuid() == mac.getUuid()){
                s.setMarka(mac.getMarka());
                s.setKvaliteta(mac.getKvaliteta());
                s.setCijena(mac.getCijena());
                return true;
            }
        }
        database.add(mac);
        return false;
    }

    @Override
    public void remove(Mac mac) {
        database.remove(mac);
    }

    @Override
    public List<Mac> getAllMacs() {
        log.warning("GetAllMacs have been called!");
        return database;
    }

}
