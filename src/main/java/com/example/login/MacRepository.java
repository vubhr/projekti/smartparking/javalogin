package com.example.login;

import java.util.List;
import java.util.UUID;

public interface MacRepository {
    Mac get(UUID id);

    void add(Mac mac);

    boolean update(Mac mac);

    void remove(Mac mac);

    List<Mac> getAllMacs();

}