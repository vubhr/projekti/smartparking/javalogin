package com.example.login;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;


import java.io.*;
import java.nio.file.Files;
import java.util.List;
import java.util.UUID;

public class StartMenuController extends ITshop  {

    PCDatabase pcoviDB = new PCDatabase();
    MacDatabase macoviDB = new MacDatabase();
    LaptopDatabase laptopiDB = new LaptopDatabase();

    List<PC> pcovi = pcoviDB.getAllPCs();
    List<Mac> macovi = macoviDB.getAllMacs();
    List<Laptop> laptopi = laptopiDB.getAllLaptops();


    @FXML
    public Label prikazPc;

    @FXML
    public TextField unosMarke;

    @FXML
    public TextField unosCijene;

    @FXML
    public TextField unosKvalitete;

    @FXML
    public Label prikazLaptop;

    @FXML
    public Label prikazMac;

    @FXML
    public Label info;

    public String pcs;
    public String macs;
    public String laptops;


    public void updateStanje(){
        prikaziPC();
        prikaziLaptop();
        prikaziMac();
    }

    public void prikaziPC(){
        pcs = "";
        for (PC pc : pcovi){
            pcs += pc.toString() +"\n";
        }
        prikazPc.setText(pcs);
    }

    public void prikaziMac(){
        macs = "";
        for (Mac mac : macovi){
            macs += mac.toString() +"\n";
        }
        prikazMac.setText(macs);
    }

    public void prikaziLaptop(){
        laptops = "";
        for (Laptop laptop : laptopi){
            laptops += laptop.toString() +"\n";
        }
        prikazLaptop.setText(laptops);
    }

    public void primjeniPopuste(){
        for (Laptop laptop : laptopi){
            if (laptop.naPopust()) {
                laptop.staviPopust();
            }
        }
        for (PC pc : pcovi){
            if (pc.naPopust()) {
                pc.staviPopust();
            }
        }
        for (Mac mac : macovi){
            if (mac.naPopust()) {
                mac.staviPopust();
            }
        }
        prikaziLaptop();
        prikaziMac();
        prikaziPC();
    }

    public void unesiPC(){
        PC pc = new PC(UUID.randomUUID(),unosMarke.getText(),Double.parseDouble(unosCijene.getText()), Integer.parseInt(unosKvalitete.getText()));

        Add dodajPC = () -> {
            pcoviDB.add(pc);
        };
        Insert(dodajPC);
        updateStanje();
    }
    public void unesiMac(){
        Mac mac = new Mac(UUID.randomUUID(),"Apple",Double.parseDouble(unosCijene.getText()), Integer.parseInt(unosKvalitete.getText()));
        Add dodajMAC = () -> {
            macoviDB.add(mac);
        };
        Insert(dodajMAC);
        updateStanje();
    }
    public void unesiLaptop(){
        Laptop laptop = new Laptop(UUID.randomUUID(),unosMarke.getText(),Double.parseDouble(unosCijene.getText()), Integer.parseInt(unosKvalitete.getText()));
        Add dodajLaptop = () -> {
            laptopiDB.add(laptop);
        };
        Insert(dodajLaptop);
        updateStanje();
    }

    public void update(){
        for (PC pc : pcovi){
            if (unosMarke.getText().equals(pc.getMarka())){
                PC novi = new PC(pc.getUuid(),unosMarke.getText(),Double.parseDouble(unosCijene.getText()), Integer.parseInt(unosKvalitete.getText()));
                pcoviDB.update(novi);
            }
        }
        for (Mac mac : macovi){
            if (unosMarke.getText().equals(mac.getMarka())){
                Mac novi = new Mac(mac.getUuid(),unosMarke.getText(),Double.parseDouble(unosCijene.getText()), Integer.parseInt(unosKvalitete.getText()));
                macoviDB.update(novi);
            }
        }
        for (Laptop laptop : laptopi){
            if (unosMarke.getText().equals(laptop.getMarka())){
                Laptop novi = new Laptop(laptop.getUuid(),unosMarke.getText(),Double.parseDouble(unosCijene.getText()), Integer.parseInt(unosKvalitete.getText()));
                laptopiDB.update(novi);
            }
        }
        updateStanje();
    }


    public void obrisi(){
        for (PC pc : pcovi){
            if (unosMarke.getText().equals(pc.getMarka())){
                Remove makni = () -> {
                    pcoviDB.remove(pc);
                };
                Remove(makni);
                updateStanje();
            }
        }
        for (Mac mac : macovi){
            if (unosMarke.getText().equals(mac.getMarka())){
                Remove makni = () -> {
                    macoviDB.remove(mac);
                };
                Remove(makni);
                updateStanje();
            }
        }
        for (Laptop laptop : laptopi){
            if (unosMarke.getText().equals(laptop.getMarka())){
                Remove makni = () -> {
                    laptopiDB.remove(laptop);
                };
                Remove(makni);
                updateStanje();
            }
        }

    }


    public void start(){

        BufferedReader reader = null;
        File file = new File("C:\\Users\\Frki\\Desktop\\java.txt");
        boolean postoji= file.exists();

        try {
            if (postoji == true) {
                reader = new BufferedReader(new FileReader(file));
                String line;

                while ((line = reader.readLine()) != null) {
                    String[] zapis = line.split(";");
                    UUID uuid = UUID.fromString(zapis[4]);
                    String marka = zapis[1];
                    double cijena = Double.parseDouble(zapis[2]);
                    int kvaliteta = Integer.parseInt(zapis[3]);
                    if (zapis[0].equals("laptop")) {
                        baza.add(new Laptop(uuid,marka, cijena, kvaliteta));
                    } else if (zapis[0].equals("pc")) {
                        baza.add(new PC(uuid,marka, cijena, kvaliteta));
                    } else if (zapis[0].equals("mac")) {
                        baza.add(new Mac(uuid,"Apple",cijena, kvaliteta));
                    } else {
                        System.out.println("Krivi format teksta");
                    }
                }
                reader.close();
            }
            else {

                FileWriter file1;
                file1 = new FileWriter("C:\\Users\\Frki\\Desktop\\java.txt");
                BufferedWriter write = new BufferedWriter(file1);

                Laptop laptop0 = new Laptop(UUID.randomUUID(),"Acer", 1999.99, 5);
                Laptop laptop1 = new Laptop(UUID.randomUUID(),"Asus", 1499.99, 4);
                Laptop laptop2 = new Laptop(UUID.randomUUID(),"Huawei", 1299.99, 3);
                Laptop laptop3 = new Laptop(UUID.randomUUID(),"Alien", 999.99, 2);
                Laptop laptop4 = new Laptop(UUID.randomUUID(),"Jana", 99.99, 1);

                PC pc0 = new PC(UUID.randomUUID(),"Corsair", 5252.55, 10);
                PC pc1 = new PC(UUID.randomUUID(),"Hyperx", 4233.66, 9);
                PC pc2 = new PC(UUID.randomUUID(),"AOC", 3568.22, 8);
                PC pc3 = new PC(UUID.randomUUID(),"Logitech", 2252.55, 7);
                PC pc4 = new PC(UUID.randomUUID(),"Master", 252.55, 6);

                Mac mac0 = new Mac(UUID.randomUUID(),"Apple",5000.59, 7);
                Mac mac1 = new Mac(UUID.randomUUID(),"Apple",4000.52, 5);
                Mac mac2 = new Mac(UUID.randomUUID(),"Apple",3000.57, 4);
                Mac mac3 = new Mac(UUID.randomUUID(),"Apple",2000.54, 3);
                Mac mac4 = new Mac(UUID.randomUUID(),"Apple",1000.51, 2);

                baza.add(laptop0);
                baza.add(laptop1);
                baza.add(laptop2);
                baza.add(laptop3);
                baza.add(laptop4);
                baza.add(pc0);
                baza.add(pc1);
                baza.add(pc2);
                baza.add(pc3);
                baza.add(pc4);
                baza.add(mac0);
                baza.add(mac1);
                baza.add(mac2);
                baza.add(mac3);
                baza.add(mac4);

                for (int i = 0; i < baza.size(); i++) {
                    write.write((baza.get(i)).prikazi() + "\n");
                }

                write.close();


            }
            for (int i=0;i<baza.size();i++){
                System.out.println(baza.get(i).prikazi());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void Insert(Add unos){ unos.dodaj(); }

    public void Remove(Remove brisi){ brisi.makni();};


}

