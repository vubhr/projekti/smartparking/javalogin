package com.example.login;
import java.util.UUID;



public abstract class Item {
    protected UUID uuid;
    protected String marka;
    protected double cijena;
    protected int kvaliteta;


    Item(UUID uuid, String marka, double cijena, int kvaliteta){
        this.uuid = uuid;
        this.cijena = cijena;
        this.marka = marka;
        this.kvaliteta = kvaliteta;
    }


    public  abstract boolean naPopust();
    public  abstract double staviPopust();

    public double getCijena() {
        return cijena;
    }

    public int getKvaliteta() {
        return kvaliteta;
    }

    public String getMarka() {
        return marka;
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setCijena(double cijena) {
        this.cijena = cijena;
    }

    public void setKvaliteta(int kvaliteta) {
        this.kvaliteta = kvaliteta;
    }

    public void setMarka(String marka) {
        this.marka = marka;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }
}
