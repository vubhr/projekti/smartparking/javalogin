package com.example.login;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.IOException;

public class LoginController {
    @FXML
    private TextField username;
    @FXML
    private Button btnLogin;
    @FXML
    private PasswordField password;
    @FXML
    private Label infoLabel;

    private Parent root;
    private Stage stage;
    private Scene scene;

    @FXML
    void onLoginClicked(ActionEvent event) throws IOException{

        String user =username.getText();

        if (username.getText().equalsIgnoreCase("admin")
                && password.getText().equalsIgnoreCase("admin")){
            FXMLLoader loader = new FXMLLoader(getClass().getResource("Show.fxml"));
            root = loader.load();
            StartMenuController startMenuController = loader.getController();
            startMenuController.start();
            stage = (Stage)((Node)event.getSource()).getScene().getWindow();
            scene = new Scene(root,600,600);
            stage.setScene(scene);
            stage.show();
        }else{
            infoLabel.setText("Nije ispravan password");


        }
    }
}
