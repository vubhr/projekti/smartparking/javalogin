package com.example.login;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;


public class PCDatabase implements PcRepository{

    static Logger log = Logger.getLogger("Models/Database/PcDatabase.java");

    List<PC> database = new ArrayList<PC>();

    public PCDatabase(){
        FileHandler fileHandler;
        try {
            fileHandler = new FileHandler("PCDatabase.log");
            log.addHandler(fileHandler);
            SimpleFormatter formatter = new SimpleFormatter();
            fileHandler.setFormatter(formatter);
            log.info("Logger initialised");
        } catch (IOException e) {
            e.printStackTrace();
        }
        log.info("Database init!");

        PC pc0 = new PC(UUID.randomUUID(),"Corsair", 5252.55, 10);
        PC pc1 = new PC(UUID.randomUUID(),"Hyperx", 4233.66, 9);
        PC pc2 = new PC(UUID.randomUUID(),"AOC", 3568.22, 8);
        PC pc3 = new PC(UUID.randomUUID(),"Logitech", 2252.55, 7);
        PC pc4 = new PC(UUID.randomUUID(),"Master", 252.55, 6);

        database.add(pc0);
        database.add(pc1);
        database.add(pc2);
        database.add(pc3);
        database.add(pc4);
    }

    @Override
    public PC get(UUID id){
        for(PC s :database){
            if(s.getUuid().compareTo(id)==0){
                return s;
            }
        }
        return null;
    }

    @Override
    public void add(PC pc) {
        database.add(pc);
    }

    @Override
    public boolean update(PC pc) {
        for (PC s : database){
            if(s.getUuid() == pc.getUuid()){
                s.setMarka(pc.getMarka());
                s.setKvaliteta(pc.getKvaliteta());
                s.setCijena(pc.getCijena());
                return true;
            }
        }
        database.add(pc);
        return false;
    }

    @Override
    public void remove(PC pc) {
        database.remove(pc);
    }

    @Override
    public List<PC> getAllPCs() {
        log.warning("GetAllPCs have been called!");
        return database;
    }



}
