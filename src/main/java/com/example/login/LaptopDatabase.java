package com.example.login;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;


public class LaptopDatabase implements LaptopRepository{

    static Logger log = Logger.getLogger("Models/Database/LaptopDatabase.java");

    List<Laptop> database = new ArrayList<Laptop>();

    public LaptopDatabase(){
        FileHandler fileHandler;
        try {
            fileHandler = new FileHandler("LaptopDatabase.log");
            log.addHandler(fileHandler);
            SimpleFormatter formatter = new SimpleFormatter();
            fileHandler.setFormatter(formatter);
            log.info("Logger initialised");
        } catch (IOException e) {
            e.printStackTrace();
        }
        log.info("Database init!");

        Laptop laptop0 = new Laptop(UUID.randomUUID(),"Acer", 1999.99, 5);
        Laptop laptop1 = new Laptop(UUID.randomUUID(),"Asus", 1499.99, 4);
        Laptop laptop2 = new Laptop(UUID.randomUUID(),"Huawei", 1299.99, 3);
        Laptop laptop3 = new Laptop(UUID.randomUUID(),"Alien", 999.99, 2);
        Laptop laptop4 = new Laptop(UUID.randomUUID(),"Jana", 99.99, 1);

        database.add(laptop0);
        database.add(laptop1);
        database.add(laptop2);
        database.add(laptop3);
        database.add(laptop4);
    }

    @Override
    public Laptop get(UUID id){
        for(Laptop s :database){
            if(s.getUuid().compareTo(id)==0){
                return s;
            }
        }
        return null;
    }

    @Override
    public void add(Laptop laptop) {
        database.add(laptop);
    }

    @Override
    public boolean update(Laptop laptop) {
        for (Laptop s : database){
            if(s.getUuid() == laptop.getUuid()){
                s.setMarka(laptop.getMarka());
                s.setKvaliteta(laptop.getKvaliteta());
                s.setCijena(laptop.getCijena());
                return true;
            }
        }
        database.add(laptop);
        return false;
    }

    @Override
    public void remove(Laptop laptop) {
        database.remove(laptop);
    }

    @Override
    public List<Laptop> getAllLaptops() {
        log.warning("GetAllLaptops have been called!");
        return database;
    }

}
