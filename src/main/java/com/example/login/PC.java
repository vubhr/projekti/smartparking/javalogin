package com.example.login;

import java.util.UUID;

public class PC extends Item implements iPrikaz{
    protected String tip = "pc";

    @Override
    public String prikazi(){
        return tip + ";" + marka + ";" + cijena + ";" + kvaliteta+";"+uuid;
    }

    public PC(UUID uuid, String marka, double cijena, int kvaliteta){
        super(uuid,marka,cijena,kvaliteta);
        this.marka = marka;
        this.cijena = cijena;
        this.kvaliteta = kvaliteta;
    }

    @Override
    public boolean naPopust() {
        if (getKvaliteta()<7 || getCijena() > 1000){
            return true;
        }
        else {
            return false;
        }
    }

    @Override
    public double staviPopust() {
        setCijena(getCijena()*0.8);
        return getCijena();
    }

    @Override
    public String toString() {
        return
                "Marka = "+marka + "\n" + "Cijena = " + String.format("%.2f",cijena) + "\n" + "Kvaliteta = " + kvaliteta + "\n";
    }

}
