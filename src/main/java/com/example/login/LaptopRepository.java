package com.example.login;

import java.util.List;
import java.util.UUID;

public interface LaptopRepository {
    Laptop get(UUID id);

    void add(Laptop laptop);

    boolean update(Laptop laptop);

    void remove(Laptop laptop);

    List<Laptop> getAllLaptops();

}