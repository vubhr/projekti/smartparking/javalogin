package com.example.login;

import java.util.List;
import java.util.UUID;

public interface PcRepository {
    PC get(UUID id);

    void add(PC pc);

    boolean update(PC pc);

    void remove(PC pc);

    List<PC> getAllPCs();

}
